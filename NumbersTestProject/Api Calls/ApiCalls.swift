//
//  ApiCalls.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation


public class ApiCalls {
    
    enum NetworkingResult {
        case fail(Error)
        case success(Data)
    }
    
    static func getData(_ url:String,_ completion: @escaping (NetworkingResult) -> Void) {
        let escapedUrl = url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlFragmentAllowed)!
        let theUrl = URL(string: escapedUrl)!
        
        var request = URLRequest(url: theUrl)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.fail(error))
            } else if let data = data {
                completion(.success(data))
            }
            }.resume()
    }
}

