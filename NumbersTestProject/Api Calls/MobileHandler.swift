//
//  MobileHandler.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation


class MobileHandler {
    
  
    static func getAllNumbers(_ completion: @escaping (Array<Number>) -> Void) {
        ApiCalls.getData(MASTER_URL) { result in
            switch result {
            case .fail(let error):
                print("error:",error)
                completion(Array<Number>())
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let numbers = try decoder.decode([Number].self, from: data)
                    
                    completion(numbers)
                }catch {
                    print("error:",error)
                    completion(Array<Number>())
                }
            }
        }
    }
    
    
    static func getNumberByName(_ name:String, _ completion: @escaping (Number) -> Void) {

        let url = "\(DETAIL_URL)\(name)"

        ApiCalls.getData(url) { result in
            switch result {
            case .fail(let error):
                print("error:",error)
                completion(Number())
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let userDetails = try decoder.decode(Number.self, from: data)

                    completion(userDetails)
                }catch {
                    print("error:",error)
                    completion(Number())
                }
            }
        }
    }
}
