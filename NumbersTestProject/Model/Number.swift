//
//  Number.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation


class Number:Codable {
    
    var name : String?
    var numberImg : String?
    var text : String?
    
    private enum CodingKeys: String, CodingKey {
        //case createdDate
        case name = "name"
        case numberImg = "image"
        case text = "text"
    }
}
