//
//  NumberCell.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import UIKit
import SDWebImage

class NumberCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var numberImg: RadiusImage!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.backgroundColor = UIColor.white
        if selected {
            self.backgroundColor = UIColor.red
        }
    }
    func setNumber (_ number: Number){
        nameLbl.text = number.name ?? ""
        
        numberImg.sd_setImage(with: URL(string: number.numberImg ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        reload()
    }
    func reload() {
        if isSelected {
            contentView.backgroundColor = UIColor.red
            self.nameLbl.textColor = UIColor.white
        }
        else if isHighlighted{
            contentView.backgroundColor = UIColor.blue
            self.nameLbl.textColor = UIColor.white
        }
        else {
            contentView.backgroundColor = UIColor.white
            self.nameLbl.textColor = UIColor.black
        }
    }
}

