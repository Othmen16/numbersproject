//
//  MasterViewController.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/18.
//  Copyright © 2018 Othmen. All rights reserved.
//

import UIKit
var numberDetail = DetailsNumberViewController()

class MasterViewController: UITableViewController {
    var numbers = Array<Number>()
    enum UIUserInterfaceIdiom : Int {
        case unspecified
        case phone // iPhone and iPod touch style UI
        case pad // iPad style UI
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        numberList = self
        
        MobileHandler.getAllNumbers { numbers in
            DispatchQueue.main.async {
                self.numbers = numbers
                numberDetail.textLbl.text = self.numbers[0].name
                numberDetail.numberImg.sd_setImage(with: URL(string: self.numbers[0].numberImg ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.tableView.reloadData()
            }
        }

        tableView.register(UINib(nibName: "NumberCell", bundle: nil), forCellReuseIdentifier: "NumberCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !Reachability.isConnectedToNetwork(){
            let alert = UIAlertController(title: "No internet connection", message: "Please try to reconnect.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
      
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let numberCell = tableView.dequeueReusableCell(withIdentifier: "NumberCell", for: indexPath) as! NumberCell
        numberCell.setNumber(self.numbers[indexPath.row])
        return numberCell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbers.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: false)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "detailsNumberViewController") as? DetailsNumberViewController)!
        
        numberDetail.textLbl.text = self.numbers[indexPath.row].name
        numberDetail.numberImg.sd_setImage(with: URL(string: self.numbers[indexPath.row].numberImg ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            if(UIDevice.current.orientation.isPortrait){
                vc.name = self.numbers[indexPath.row].name
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case .pad:break
        case .unspecified:break
        case .tv: break
        case .carPlay: break
            
        }
    }
   
}
