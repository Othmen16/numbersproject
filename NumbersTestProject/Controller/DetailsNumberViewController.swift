//
//  DetailsNumberViewController.swift
//  NumbersTestProject
//
//  Created by nino on 10/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import UIKit
var numberList = MasterViewController()

class DetailsNumberViewController: UIViewController {
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var numberImg: UIImageView!
    var name:String?
    var currList = MasterViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
         numberDetail = self
        // Do any additional setup after loading the view.
        if let name = self.name {
            MobileHandler.getNumberByName(name) { number in
                DispatchQueue.main.async{
                    self.textLbl.text = number.text ?? ""
                    self.numberImg.sd_setImage(with: URL(string: number.numberImg ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                }
            }
        }
    }
    
}
